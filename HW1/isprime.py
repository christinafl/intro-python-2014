""" Christina Floristean
    Intro to Comp
    Adam Cannon
    September 22, 2014
    Assignment 1"""

# Input a number. 
num = input("Please enter a number: ")

# Check for special cases, such as if the number is less than or
# equal to one or if it is two. Also check to see if the number is 
# even to avoid unnecessary calculations later on. 
if num <= 1:
    print('This is not a valid input. ')
elif num == 2:
    print('This number is prime. ')
elif num%2 == 0:
    print('This number is not prime. ')
else:
    notfound = True
    i = 3
    
    # Stop checking for factors when the square root of that number is
    # reached since neither factor can be bigger than this number.
    stop = num ** 0.5
    
    while i < stop+1 and notfound:
        
        # If the number is divisible by i,the number is not prime and 
        # the the program exits the loop. 
        if num%i == 0:
            notfound = False
        else:
            notfound = True
        
        # Add two each time to skip all the even numbers. 
        i += 2
        
    #Print the result.
    if notfound:
        print('This number is prime. ')
    else:
        print('This number is not prime. ')
        