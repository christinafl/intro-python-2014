""" Christina Floristean
    Intro to Comp
    Adam Cannon
    September 22, 2014
    Assignment 1"""
    
import datetime

# Input the data as a string with spaces between each value. 
dob = raw_input("Please enter your date of birth in the format yyyy mm dd: ")

# Create date object with the current date.
now = datetime.date.today()

# Split and store the year, month, and date from the input string. 
dob_list = dob.split()

# Create new date object with the input data. 
bday = datetime.date(int(dob_list[0]), int(dob_list[1]), int(dob_list[2]))

# Create timedelta object by taking the difference between the 
# two dates.
diff = now-bday

# Print the result
print('Your age in days is ' + str(diff.days))
