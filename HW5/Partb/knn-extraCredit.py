# *******************************************************
# Christina Floristean
# cf2469
# Knn Tester - Extra Credit
# Assignment 5 Part 2
# ENGI E1006
# *******************************************************

import Knn

def main():
    p = 10
    
     # Catalog the distance functions.
    dist = { 1:Knn.e_distance, 2:Knn.bc_distance, 3:Knn.cm_distance }
    
    k = input("Enter the neighborhood size: ")\
    
    # Present user with the options.
    print "\nDistance Options:"
    print "[1] Euclidean Distance"
    print "[2] Bray-Curtis Dissimilarity"
    print "[3] Center of Mass Distance"
    
    go_again = True
    
    while go_again:
        choice = input("\nEnter the number of the chosen distance formula: ")
        if int(choice) <= 0 or int(choice) >3:
            print "That is not a valid entry, please try again."
        else:
            go_again = False
    
    data = Knn.make_data('wdbc.data')
    print "Accuracy is ...", Knn.n_validator(data, p, Knn.KNNclassifier, k, dist[choice]) 
    
main()