# *******************************************************
# Christina Floristean
# cf2469
# Knn Tester
# Assignment 5 Part 2
# ENGI E1006
# *******************************************************

import Knn

def main():
    # Number of partitions.
    p = 10
    k = input("Enter the neighborhood size: ")
    
    # Get data from file. 
    data = Knn.make_data('wdbc.data')
    print "Accuracy is ...", Knn.n_validator(data, p, Knn.KNNclassifier, k, Knn.e_distance) 
    
main()