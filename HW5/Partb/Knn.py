# *******************************************************
# Christina Floristean
# cf2469
# Knn
# Assignment 5 Part 2
# ENGI E1006
# *******************************************************

import numpy as np
import math

def make_data(file_name):
    """Reads a matrix of sample data

    file_name is the input file name
    """
    
    # Read file. 
    # Get the size from the first line. 
    inf = open(file_name, "r")
    
    # Fill an empty numPy array with zeroes. 
    a = []
    # Fill the array with the values in from the file. 
    for row in inf:
        values = row.split(',')
        nVals = values[1:]
        if values[1] == 'B':
            nVals[0] = 0
        else:
            nVals[0] = 1
        nVals.extend( [float(x) for x in values[2:]])
        a.append(nVals)

    # Close the file. 
    inf.close()
    
    return np.array(a)

def split_data(data, p, n):
    """Splits sample data into training and testing 

    data is the sample data, p is the number of partitions to use
    and 0 <= n < p designates which partition to use as test
    returns the tuple of training and testing matrices
    """

    # Total number of samples.
    nsamples = np.shape(data)[0]
    # Find number of tests in the last partition.
    ntlast = nsamples/p if nsamples % p == 0 else nsamples - (nsamples/p + 1) * (p-1) 
    # Find number of tests in any other partition.
    nt = (nsamples - ntlast) / (p-1)
    
    # First partition is test.
    if n == 0:
        return (data[nt:], data[:nt])
    # Last partition is test.
    elif n == p-1:
        return (data[:-ntlast], data[-ntlast:])
    # General case.
    else:
        return (np.vstack((data[:(n*nt)],data[(n+1)*nt:])), data[n*nt:(n+1)*nt])


def e_distance(x, y):
    """ Calculate the Euclidian distance between two lists of coordinates
    
    test and training are the two data sets whose distance needs to be measured
    """
    # Convert string array to float array.
    x = x.astype(np.float)
    y = y.astype(np.float)
    d = 0
    
    #  Find the distance between corresponding values in training and testing. 
    for i in range(len(x)):
        d = d + (x[i] - y[i])**2
        
    return math.sqrt(d)        


def cm_distance(x, y):
    """ Calculate the distance between two lists of coordinates 
    by measuring the distance between their centers of mass
    
    test and training are the two data sets whose distance needs to be measured
    """
    # Convert string array to float array.
    x = x.astype(np.float)
    y = y.astype(np.float)
    cm1 = 0
    
    # Find the distance between their respective centers of mass.
    for i in range(len(x)):
        cm1 = cm1 + x[i]
       
    cm2 = 0 
    for i in range(len(y)):
        cm2 = cm2 + y[i]
        
    return abs(cm1/len(x) - cm2/len(y)) 


def bc_distance(x, y):
    """ Calculate the Bray-Curtis distance between two lists of coordinates 
    
    test and training are the two data sets whose distance needs to be measured
    """
    # Convert string array to float array.
    x = x.astype(np.float)
    y = y.astype(np.float)
    num = 0
    denom = 0
    
    for i in range(len(x)):
        num = num + abs(x[i] - y[i])
        denom = denom + x[i] + y[i]
        
    return num/denom 

def NNclassifier(training, test):
    """ Calculate the label of set of test samples using the nearest neighbor algorithm
    
    Training and test are the two data sets for the algorithm
    """
    
    labels = []
    
    # For each unknown label, find the data entry with the data that closest
    # resembles the unlabeled data entry.
    for i in test:
        smallest_d = -1
        label = -1
        
        # Go through all the labeled data.
        for k in training:
            d = e_distance(i, k[1:])
            
            # Find the closest data or the one with the smallest distance from the
            # unlabeled data point in space.
            if smallest_d < 0 or d < smallest_d:
                smallest_d = d
                label = k[0]
        labels.append(label)
    return np.array(labels)

def KNNclassifier(training, test, k, distance):
    """ Calculate the label of set of test samples using the k nearest neighbor algorithm
    
    Training and test are the two data sets for the algorithm, k is the neighbor depth
    """    
    # An array of tuples (distance, label).
    dist_labels = []
    
    # For each unknown label, find the data entry with the data that closest
    # resembles the unlabeled data entry.
    for i in test:
        
        # All neighbors.
        n_neighbors = []
        
        # Nearest k neighbors. 
        chosen = []
        
        # Go through all the labeled data.
        for l in training:
            d = distance(i, l[1:])
            n_neighbors.append((d, l[0]))
            
        # Sort by distance from smallest to largest.
        n_neighbors.sort()
        chosen = n_neighbors[:k]
        
        # Find out which label occurs most frequently of the chosen neighbors. 
        dist_labels.append(max(set(chosen), key=chosen.count))
    
    # Store the label only, not the distance. 
    labels = [x[1] for x in dist_labels]
    return np.array(labels)

def n_validator(data, p, classifier, *args):
    """ Validate a classifier by comparing the calculated labels with the actual ones
    
    data is the sample set, p is the number of partitions to use, classifier is the
    classifier function to validate and args are classifier arguments
    """
    # Shuffle sample data.
    np.random.shuffle(data)
    
    # Call the classifier on each of the p partitions.
    total = 0.0
    for i in range(p):
        # Test is i'th partition.
        (training, test) = split_data(data, p, i)
        labels = classifier(training, test[:,1:], args[0], args[1])
        
        score = 0
        
        # Determine how many times the correct label is chosen. 
        for j in range(len(labels)):
            if labels[j] == test[j,0]:
                score = score + 1
        
        total += score
    
    # Determine percent accuracy.  
    return total/np.shape(data)[0] 
    
