# *******************************************************
# Christina Floristean
# cf2469
# Knn Tester
# Assignment 5 Part 1
# ENGI E1006
# *******************************************************

import Knn

def main():
    p = 5
    (training,test) = Knn.split_data(Knn.make_data('wdbc.data'), p)
    labels = Knn.NNclassifier(training, test[:,1:])
    print labels
main()