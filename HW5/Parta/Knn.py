# *******************************************************
# Christina Floristean
# cf2469
# Knn
# Assignment 5 Part 1
# ENGI E1006
# *******************************************************

import numpy as np
import math

def make_data(file_name):
    """Reads a matrix of sample data

    file_name is the input file name
    """
    
    # Read file. 
    # Get the size from the first line. 
    inf = open(file_name, "r")
    
    # Fill an empty numPy array with zeroes. 
    a = []
    # Fill the array with the values in from the file. 
    for row in inf:
        values = row.split(',')
        nVals = values[1:]
        if values[1] == 'B':
            nVals[0] = 0
        else:
            nVals[0] = 1
        nVals.extend( [float(x) for x in values[2:]])
        a.append(nVals)

    # Close the file. 
    inf.close()
    
    return np.array(a)

def split_data(data, p):
    """Splits sample data into training and testing 

    data is the sample data and p is the number of partitions to use
    returns the tuple of training and testing matrices
    """
   
    # Separate the data into two portions given by p. 
    split_index = math.ceil((p-1.0)/p * np.shape(data)[0])
    
    training = data[:split_index+1]
    testing = data[split_index+1:]
    return (training, testing)

def distance(x, y):
    """ Calculate the Euclidian distance between two lists of coordinates
    
    test and training are the two data sets whose distance needs to be measured
    """

    d = 0
    
    # Find the distance between corresponding values in training and testing. 
    for i in range(len(x)):
        d += (float(x[i]) - float(y[i])) ** 2
        
    return math.sqrt(d)

def NNclassifier(training, test):
    """ Calculate the label of set of test samples using the nearest neighbor algorithm
    
    Training and test are the two data sets for the algorithm
    """
    
    labels = []
    
    # For each unknown label, find the data entry with the data that closest
    # resembles the unlabeled data entry.
    for i in test:
        smallest_d = -1
        label = -1
        
        # Go through all the labeled data.
        for k in training:
            d = distance(i, k[1:])
            
            # Find the closest data or the one with the smallest distance from the
            # unlabeled data point in space.
            if smallest_d < 0 or d < smallest_d:
                smallest_d = d
                label = k[0]
        labels.append(label)
    return np.array(labels)
     
