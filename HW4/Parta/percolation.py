# *******************************************************
# Christina Floristean
# cf2469
# Percolation Module
# Assignment 4 Part 1
# ENGI E1006
# *******************************************************

import numpy as np
import sys
import random

def read_grid(filename):
    """Create a site vacancy matrix from a text file.

    filename is a String that is the name of the 
    text file to be read. The method should return 
    the corresponding site vacancy matrix represented
    as a numpy array
    """

    # Read file. 
    # Get the size from the first line. 
    inf = open(filename, "r")
    size = int(inf.readline())
    
    # Fill an empty numPy array with zeroes. 
    a = np.zeros((size, size), dtype=int)
    
    # Fill the array with the values in from the file. 
    for i in range(size):
        values = inf.readline().split()
        if len(values) != size:
            sys.exit("Input file has inconsistent format!")
        a[i] = [int(x) for x in values]
    
    # Close the file. 
    inf.close()
    return a


def write_grid(filename,sites):
    """Write a site vacancy matrix to a file.

    filename is a String that is the name of the
    text file to write to. sites is a numpy array
    representing the site vacancy matrix to write
    """
    
    shape = sites.shape
    if shape[0] != shape[1]:
        sys.exit("Matrix to be written is not square!")
    size = shape[0]
    
    # Write to file. 
    outf = open(filename, "w")
    
    # Write the header
    outf.write(str(size))
    outf.write("\n")
    
    # Write the values from the array into the file. 
    for i in range(size):
        for j in range(size):
            outf.write(str(sites[i,j]))
            outf.write(" ")
        outf.write("\n")
    
    # Close the file. 
    outf.close()


def flow(sites):
    """Returns a matrix of vacant/full sites (1=full, 0=vacant)

    sites is a numpy array representing a site vacancy matrix. This 
    function should return the corresponding flow matrix generated 
    through vertical percolation
    """
    cols = sites.shape[1]
    
    # Create a new numPy array consisting of all zeroes. 
    flow = np.zeros(sites.shape, dtype=int)
    
    # Fill the flow matrix. 
    for i in range(cols):
        # Get each column. 
        curr_col = sites[:,i]
        j = 0
        
        # Traverse through the numbers in each column. 
        while j < len(curr_col):
           
            # If a zero is found, the rest of the column will be all zeros.
            # Let j be the end condition to stop it from continuing down the column. 
            if curr_col[j] == 0:  
                j = len(curr_col)
           
            # If there is a 1, put it in the flow matrix and keep going. 
            else:
                flow[j,i] = 1
                j += 1         
    return flow
    
    


def percolates(flow_matrix):
    """Returns a boolean if the flow_matrix exhibits percolation

    flow_matrix is a numpy array representing a flow matrix
    """
    
    # Obtain bottom row. 
    bottom_row = flow_matrix[-1]
    for i in range(len(bottom_row)):
        
        # If any of the bottom numbers is a 1, then it percolates. 
        if bottom_row[i] == 1:
            return True
    return False
    
def make_sites(p,n):
    """Returns an nxn site vacancy matrix

    Generates a numpy array representing an nxn site vacancy 
    matrix with site vaccancy probability p
    """
    # Make a numPy array of all zeroes. 
    a = np.zeros((n, n), dtype=int)
   
    # Fill the matrix.  
    for i in range(n):
        for j in range(n):
            r = random.random()
            
            # If the random number is less than the probability, the site is open. 
            if r <= p:
                a[i,j] = 1
            
    return a
