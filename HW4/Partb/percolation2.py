# *******************************************************
# Christina Floristean
# cf2469
# Percolation Module
# Assignment 4 Part 2
# ENGI E1006
# *******************************************************

import numpy as np
import sys
import random
import pylab as plt

def read_grid(filename):
    """Create a site vacancy matrix from a text file.

    filename is a String that is the name of the 
    text file to be read. The method should return 
    the corresponding site vacancy matrix represented
    as a numpy array
    """

    # Read file. 
    # Get the size from the first line. 
    inf = open(filename, "r")
    size = int(inf.readline())
    
    # Fill an empty numPy array with zeroes. 
    a = np.zeros((size, size), dtype=int)
    
    # Fill the array with the values in from the file. 
    for i in range(size):
        values = inf.readline().split()
        if len(values) != size:
            sys.exit("Input file has inconsistent format!")
        a[i] = [int(x) for x in values]
    
    # Close the file. 
    inf.close()
    return a



def write_grid(filename,sites):
    """Write a site vacancy matrix to a file.

    filename is a String that is the name of the
    text file to write to. sites is a numpy array
    representing the site vacany matrix to write
    """

    shape = sites.shape
    if shape[0] != shape[1]:
        sys.exit("Matrix to be written is not square!")
    size = shape[0]
    
    # Write to file. 
    outf = open(filename, "w")
    
    # Write the header
    outf.write(str(size))
    outf.write("\n")
    
    # Write the values from the array into the file. 
    for i in range(size):
        for j in range(size):
            outf.write(str(sites[i,j]))
            outf.write(" ")
        outf.write("\n")
    
    # Close the file. 
    outf.close()


def undirected_flow(sites):
    """Returns a matrix of vacant/full sites (1=full, 0=vacant)

    sites is a numpy array representing a site vacancy matrix. This 
    function should return the corresponding flow matrix generated 
    through undirected percolation
    """

    # Make a corresponding flow matrix to work with. 
    cols = len(sites[0])
    full = np.zeros(sites.shape, dtype=int)
    
    # Flow recursively from the first row. 
    for j in range(cols):
        flow_from(sites, full, 0, j)
        
    return full


def flow_from(sites,full,i,j):
    """Adjusts the full array for flow from a single site

    This method does not return anything. It changes the array full
    """


    (rows, cols) = sites.shape

    # Stop conditions for recursion.
    # Off the edge of the row.
    if i < 0 or i >= cols:
        return
    # Off the edge of the column.
    if j < 0 or j >= rows:
        return
    # Site is not opened, can't propagate.
    if sites[i,j] == 0:
        return
    # Already marked as full, been here before. 
    if full[i,j] == 1:
        return
    
    # Otherwise mark site as full.
    full[i][j] = 1
    
    # Mark it in sites as well in order to visualize.
    sites[i][j] = 2

    # Recursively propagate in all directions. 
    flow_from(sites, full, i+1, j);  
    flow_from(sites, full, i, j+1);   
    flow_from(sites, full, i, j-1);   
    flow_from(sites, full, i-1, j);  


def percolates(flow_matrix):
    """Returns a boolean if the flow_matrix exhibits percolation

    flow_matrix is a numpy array representing a flow matrix
    """

    # Obtain bottom row. 
    bottom_row = flow_matrix[-1]
    for i in range(len(bottom_row)):
        
        # If any of the bottom numbers is a 1, then it percolates. 
        if bottom_row[i] == 1:
            return True
    return False

def make_sites(p,n):
    """Returns an nxn site vacancy matrix

    Generates a numpy array representing an nxn site vacancy 
    matrix with site vaccancy probability p
    """
    # Make a numPy array of all zeroes. 
    a = np.zeros((n, n), dtype=int)
   
    # Fill the matrix.  
    for i in range(n):
        for j in range(n):
            r = random.random()
            
            # If the random number is less than the probability, the site is open. 
            if r <= p:
                a[i,j] = 1
            
    return a

def show_perc(sites):
    
    # Plot the matrix showing the percolation. 
    plt.matshow(sites)
    plt.show()
    
def make_plot(trials,n):
    """generates and displays a graph of percolation p vs. vacancy p

    estimates percolation probability on an nxn grid for undirected 
    percolation by running a Monte Carlo simulation using the variable trials number
    of trials for each point. 
    """

    # Number of points.
    N = 100
    
    # Vacancy probabilities.
    vprob = np.zeros(N, dtype=float)
    # Percolation probabilities.
    pprob = np.zeros(N, dtype=float)
    
    # Iterate over a range of vacancy probabilities.
    for i in range(1,N):
        p = i/float(N)
        cnt = 0
        
        # Run trials.
        for j in range(trials):
            S = make_sites(p,n)
            if percolates(undirected_flow(S)):
                cnt = cnt+1
        vprob[i] = p
        pprob[i] = cnt/float(trials)
        
    # Plot the graph.
    plt.xlabel('Site Vacancy Probability')
    plt.ylabel('Percolation Probability')
    plt.title('Percolation Probability Plot')
    plt.plot(vprob, pprob, 'b-')
    plt.axis([0, 1, 0, 1])
    plt.show()


    
     