# *******************************************************
# Christina Floristean
# cf2469
# Tweets Module Tester
# Assignment 6
# ENGI E1006
# *******************************************************

import tweets

def main():
    # Get average sentiment of some_tweets.txt.
    tweets_data = tweets.add_sentiment(tweets.make_tweets('some_tweets.txt'), 'sentiments.csv')
    print "Average sentiment: " + str(tweets.avg_sentiment(tweets_data)) + '\n'
    
    tweets.make_new_file(tweets_data, 'tweets_sentiment.txt')
    tweets_new_data = tweets.make_sent_tweets('tweets_sentiment.txt')

    # Filter tweets.
    chosen_tweets = tweets.tweet_filter(tweets_new_data,  "first", "time")
    
    # Print tweets containing words in args.
    print "The tweets containing the chosen words are: "
    for tweet in chosen_tweets:
        print tweet['text'] 
        
    print "Average sentiment of filtered tweets: " + str(tweets.avg_sentiment(chosen_tweets))

main()
