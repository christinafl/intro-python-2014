# *******************************************************
# Christina Floristean
# cf2469
# Tweets Module
# Assignment 6
# ENGI E1006
# *******************************************************

import datetime
import csv
import string

def make_tweets(inFile):
    ''' Makes a list of dictionaries that contain the 
    
    following information: text, time, longitude, and 
    latitude of the tweet. Reads a .txt file row by 
    row to obtain this information. Returns the list.
    '''
    
    all_tweets = []
    
    # Open the file. 
    inf = open(inFile, 'r')
    
    # For every tweet.
    for line in inf:
        data = line.split("\t")
        
        # Make sure data is formatted how we expect it to be. 
        if len(data) != 4:
            continue
        
        # Separate the different types of data. 
        coordinates = data[0][1:-1].split(", ")
        d = data[2].split()
        date_str = "-".join(d[:]).replace(':', "-")
        date = datetime.datetime.strptime(date_str, '%Y-%m-%d-%H-%M-%S')
        
        # Create a dictionary and add it to the list. 
        all_tweets.append({'text' : data[3].lower(), 'time' : date, 
                           'latitude' : float(coordinates[1]), 
                           'longitude' : float(coordinates[0])})
    inf.close()
    
    # Return the list.
    return all_tweets
    
def make_sent_tweets(inFile):
    ''' Create a list of dictionaries as in make_tweets
    
    except with the sentiment already added into each row 
    of the file. 
    '''
    
    # Add all the usual information as before. 
    all_tweets = []
    
    # Open the file. 
    inf = open(inFile, 'r')
    
    # For every tweet.
    for line in inf:
        data = line.split("\t")
        
        # Make sure data is formatted how we expect it to be. 
        if len(data) != 4:
            continue
        
        # Separate the different types of data. 
        if data[1] != 'None': 
            sentiment = float(data[1])
        else:
            sentiment = None
        coordinates = data[0][1:-1].split(", ")
        d = data[2].split()
        date_str = "-".join(d[:]).replace(':', "-")
        date = datetime.datetime.strptime(date_str, '%Y-%m-%d-%H-%M-%S')
        
        # Create a dictionary and add it to the list. 
        all_tweets.append({'text' : data[3].lower(), 'time' : date, 
                           'latitude' : float(coordinates[1]), 
                           'longitude' : float(coordinates[0]), 
                           'sentiment' : sentiment})
    inf.close()
    
    # Return the list.
    return all_tweets

def make_new_file(tweets, filename):
    ''' Makes a new file given the list of dictionaries 
    
    with the sentiment of tweet added.
    '''
    outf = open(filename, 'w')
    
    # Creates the same file as before, where the sentiment takes the place
    # of the 6. 
    for tweet_info in tweets:
        outf.write('[' + str(tweet_info['longitude']) + ', ' + str(tweet_info['latitude']) + ']')
        outf.write('\t')
        outf.write(str(tweet_info['sentiment']))
        outf.write('\t')
        outf.write(str(tweet_info['time']))
        outf.write('\t')
        outf.write(tweet_info['text'])
        outf.write('\n')
        
    outf.close()
     

def add_sentiment(tweets, sentiment_file):
    ''' Finds the sentiment of each word in a tweet
    
    and averages the values to find the overall
    sentiment of the tweet. Adds this to the 
    dictionary and returns the updated list. 
    '''
    
    csv_data = {}
    
    # Open and read csv file. 
    sentiments = open(sentiment_file, 'rt')
    reader = csv.reader(sentiments)
    
    # Make a dictionary from the csv file where the words are keys 
    # and the sentiments are values. 
    for row in reader:
        new_word = row[0].translate(string.maketrans("",""), string.punctuation)
        csv_data[new_word] = float(row[1])
    
    # Find the sentiment of each tweet. 
    for info in tweets:
        total = 0
        n = 0
        avg = None
        
        # Find the sentiment of each word. 
        for word in info['text'].split():
            
            # Remove punctuation. 
            word2 = word.translate(string.maketrans("",""), string.punctuation)
            
            # If a sentiment is found, add it to the total and increment the
            # number of not None sentiments. 
            if word2 in csv_data:
                total += csv_data[word2]
                n +=1
        
        # Find the average if any word was found in the list. 
        if n != 0:
            avg = total / n
        
        # Add the sentiment to the dictionary.
        info['sentiment'] = avg
      
    sentiments.close() 
    
    # Return the updated list. 
    return tweets
    
def avg_sentiment(tweets):
    ''' Finds and returns the average sentiment of
    
    all the tweets in a list.
    '''
    
    total = 0
    n = 0
    for info in tweets:
        
        # Don't include tweets with no sentiment. 
        if info['sentiment'] != None:
            total += info['sentiment']
            n += 1
    
    # Return the average. 
    return total/n
    
    
def tweet_filter(tweets,*args):
    ''' Filters a list of tweets by determining if each tweet 
    
    has all the words listed in args. 
    '''
    new_tweets = []
    
    
    filter_words = [w.translate(string.maketrans("",""), string.punctuation).lower() for w in args]
    
    for info in tweets:
        
        # Make sure the tweet is lower case and without punctuation. 
        tweet = info['text'].translate(string.maketrans("",""), string.punctuation).split()
        
        n = 0
        for w in filter_words:
            if w in tweet:
                n += 1
        
        # If the index is equal to the number of words, all of them were in the tweet. 
        if n == len(args):
            new_tweets.append(info)
    
    # Return the new list of tweets.         
    return new_tweets
    