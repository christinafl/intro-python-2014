""" Christina Floristean
    Intro to Comp
    Adam Cannon
    October 7, 2014
    Assignment 2"""
    
G = 6.6742 * 10 ** -11
Me = 5.9736 * 10 ** 24
Re = 6371000

# This function calculates gravity given a certain distance. 
def g(h):
    return G*Me / (Re + h)**2

# This function calculates distance based on the distance and velocity 
# values at the last increment. 
def s_next(s_current, v_current, delta_t):
    return s_current + v_current * delta_t

# This function calculates velocity based on the distance and velocity 
# values at the last increment. 
def v_next(s_current, v_current, delta_t):
    return v_current - g(s_current) * delta_t

# This function calculates distance at a specific time using the 
# distance and velocity formulas of s_next and v_next. 
def s_sim(t, v_init, s_init, delta_t):
    i = 0
    
    # Initialize variables v_n and s_n
    v_n = v_init
    s_n = s_init
    
    # Iterate through all values to t to find the distance at the given
    # time. 
    while i <= t:
        
        # Find distance and velocity at a specific time. 
        s = s_next(s_n, v_n, delta_t)
        v = v_next(s_n, v_n, delta_t)
        
        # Store values so that s and v coincide in next calculation.
        s_n = s
        v_n = v
        i += delta_t
    
    # Return distance
    return s

# This function calculates distance using the standard distance formula.
def s_standard(t, v_init):
    return -0.5*9.81 * t**2 + v_init*t

# This function calculates distance at each time t until ground level 
# is reached. 
def allpos(delta_t, v_init, s_init):
    t = 0
    
    # Store positions where each index is equal to the time. 
    positions = []
    
    # Initialize values. 
    v = v_init
    s = s_init
    
    # Find all distances until ground level is reached. 
    while s >= 0:
        
        # Adds distance value when new, nondecimal value of t is reached. 
        if abs(t-int(t)) < delta_t:
            positions.append(s)
            
        # Find next distance and velocity values. 
        s = s_next(s, v, delta_t)  
        v = v_next(s, v, delta_t)
       
        t+=delta_t 
    
    # Return list of positions.   
    return positions