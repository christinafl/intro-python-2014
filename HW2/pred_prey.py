""" Christina Floristean
    Intro to Comp
    Adam Cannon
    October 7, 2014
    Assignment 2"""

# This function determins the amount of prey present in the system
# when considering the variables A, B, and the amount of predators 
# present. 
def prey_next(pred_now, prey_now, A, B):
    return prey_now * (1 + A - B * pred_now)

# This function collects and returns the amount of prey left up to a
# certain iteration.   
def prey_future(pred_now, prey_now, A, B, C, D, n):
    i = 1
    prey_cur = prey_now
    pred_cur = pred_now
    
    # Loop through and find the prey present at each index until the 
    # given one is reached.  
    while (i <= n):
        
        # Find the predators and prey present at each index
        prey_cur = prey_next(pred_cur, prey_cur, A, B)
        pred_cur = pred_next(pred_cur, prey_cur, C, D)
        i += 1
    
    # Return the amount of prey left at a cerain index
    return prey_cur

# This function determins the amount of predators present in the 
# system when considering the variables C, D, and the amount of prey 
# present.  
def pred_next(pred_now, prey_now, C, D):
    return pred_now * (1 - C + D * prey_now)

# This function collects and returns the amount of predators left up 
# to a certain iteration.      
def pred_future(pred_now, prey_now, A, B, C, D, n):
    i = 1
    prey_cur = prey_now
    pred_cur = pred_now
    
    # Loop through and find the predators present at each index until 
    # the given one is reached. 
    while (i <= n):
        # Find the predators and prey present at each index
        prey_cur = prey_next(pred_cur, prey_cur, A, B)
        pred_cur = pred_next(pred_cur, prey_cur, C, D)
        i += 1
    # Return the amount of predators left at a cerain index
    return pred_cur