""" Christina Floristean
    Intro to Comp
    Adam Cannon
    October 7, 2014
    Assignment 2"""
    
import game_module

# This functions runs a simulation from each player's point of view.
def main():
    # How many rounds per session.
    rounds = 10000
    # Strategy increment.
    increment = 0.05
    
    # Player 1 is true and Player 2 is false. 
    player = True
    
    # Run simulations.
    print "Simulation for Player One: "
    game_module.run_simulation(rounds, increment, player)
    
    print
    print "Simulation for Player Two: "
    game_module.run_simulation(rounds, increment, not player)
    
#now run main()
main()