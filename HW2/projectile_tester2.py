""" Christina Floristean
    Intro to Comp
    Adam Cannon
    October 7, 2014
    Assignment 2"""
    
import projectile 

def main():
   
# Set up intitial values
    v_0 = 300
    s_0 = 0
    t=0
    delta_t = .05
    # Print a table with values computed both ways for positive positions

    print('seconds \t distance_sim \t \t distance_formula')
    print('-----------------------------------------------------------')
    
    s = projectile.allpos(delta_t,v_0,s_0)
    while t < len(s):
        s_formula = projectile.s_standard( t,v_0)  
        print( '%2d \t \t %.5f \t \t %.5f') %(t,s[t],s_formula)
        t=t+1
# now run main()
main()