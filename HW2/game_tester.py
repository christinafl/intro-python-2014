""" Christina Floristean
    Intro to Comp
    Adam Cannon
    October 7, 2014
    Assignment 2"""
    
import game_module

# This program controls user input and program output. It is the 
# "control function" for the game. 
def main():
   print "Version 1: You vs.the computer. " 
   print "Version 2: The computer vs. itself. "
   play = True
   
   # Initalize arrays that will contain data returned from the game
   # module. 
   totals1 = [0,0,0]
   totals2 = [0,0,0]
   
   # Loop runs while the use continues to want to play the game
   while play == True:
    choice = input("Enter '1' for version 1 or '2' for version 2. ")
        
    # Enters this if statement for player vs. computer. 
    
    if choice == 1:
        answer = input("You are player 2. Enter '1' or '2' as your guess. ")
            
        # Obtains results from game. 
        results = game_module.you_comp(answer, totals1)
        
        # Prints the winning player as well as each player's total
        print results[0]
        print "Player 1 total: " + str(results[1])
        print "Player 2 total: " + str(results[2])
    
    # Enters this elif statement for computer vs. computer.
    elif choice == 2:
        
        #Obtains results from game.
        results = game_module.comp_comp(totals2)
        
        # Prints the winning player as well as player's total. 
        print results[0]
        print "Player 1 total: " + str(results[1])
        print "Player 2 total: " + str(results[2])
    
    else:
        print "That is not a valid input. "
    
    # Determines if the user wants to play another round. 
    again = False
    while again == False:
        playagain = raw_input("Would you like to play another round? Type 'y' for yes or 'n' for no. ")
        if playagain == 'n':
            play = False
            print "Goodbye! "
            again = True
        elif playagain == 'y':
            play = True
            again = True
        else:
            print "That is not a valid response. "
main()
    