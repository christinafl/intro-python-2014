""" Christina Floristean
    Intro to Comp
    Adam Cannon
    October 7, 2014
    Assignment 2"""
       
import random

# This function randomly draws of 1 or 2 according to threshold 
# value t
def draw(t):
    d = random.random();
    return 1 if d <= t else 2;

# This function manages the player vs. computer version of the game.   
def you_comp(guess, totals):
    
    # Pick the threshold value. 
    t = random.random()
    
    # Determine the guess based on the threshold and add it to the 
    # player's guess
    answer = draw(t)
    result = guess+answer
    
    # Checks if the value is even.The sum is added to the winner's
    # total and subtracted from the loser's total.
    if (result)%2 == 0:
        print "You win!"
        totals[0] = "Player 2 wins $" + str(result)
        totals[2] += result
        totals[1] -= result
    
    # If the number is odd, the sum is added to the winner's total
    # and subtracted from the loser's total. 
    else:
        print "Sorry, you lose."
        totals[0] = "Player 1 wins $" + str(result)
        totals[1] += result
        totals[2] -= result
    return totals

# This function manages the computer vs. computer game.         
def comp_comp(totals):
    
    # Chooses the threshold for each computer player.
    t1 = random.random()
    t2 = random.random()
    
    # Determine each guess based on the threshold and add them together.
    answer1 = draw(t1)
    answer2 = draw(t2)
    result = answer1+answer2
    
    # Checks if the value is even.The sum is added to the winner's
    # total and subtracted from the loser's total.
    if (result)%2 == 0:
        print "Player 2 wins!"
        totals[0] = "Player 2 wins $" + str(result)
        totals[2] += result
        totals[1] -= result
    
    # If the number is odd, the sum is added to the winner's total
    # and subtracted from the loser's total.
    else:
        print "Player 1 wins!"
        totals[0] = "Player 1 wins $" + str(result)
        totals[1] += result
        totals[2] -= result
    return totals
    
# This function plays a simulated round between two computer players
# considered from the player one's point of view
def play_round1(t1, t2):
    s = draw(t1) + draw(t2)
    return -s if s % 2 == 0 else s 

# This function plays a simulated round between two computer players
# considered from player two's point of view
def play_round2(t1, t2):
    s = draw(t1) + draw(t2)
    return s if s % 2 == 0 else -s 
    
# This function runs a specified number of rounds with given a 
# threshold and calculate the average outcome from the chosen player's
# point of view          
def run_simple_simulation(t1, t2, rounds, player):
    i = 0
    s = 0;
    while i < rounds:
        if player:
            s += play_round1(t1, t2) 
        else:
            s += play_round2(t1, t2)  
        i += 1
        
    # Return average of all the rounds for the chosen player
    return s/rounds

# Run a series of simulations: for each strategy of player 1 or 2
# Average the winnings over simulations conducted for a series of simulations 
# for a set of strategies for the other player
def run_simulation(rounds, increment, player):
    t1 = 0
    
    # For incremental strategies of chosen player
    while t1 <= 1:
        s = 0
        t2 = 0
        
        # Run simulation for an incremental 
        # series (threshold values) not-chosen player
        while t2 <= 1:
            s += run_simple_simulation(t1, t2, rounds, player)
            t2 += increment
        print "Strategy ", t1, "Avg winnings ", s * increment
        t1 += increment
    