#**********************************************************
# Christina Floristean 
# cf2469
# October 23, 2014
# Assignment 3
#
#
# Description: This program is a module that contains
# many functions to alter the appearance of an image file,
# as well as functions to process the image one line of  
# pixels at a time.  
#**********************************************************

# Processing buffer size.
BUF_SIZE = 3072

# Function to negate colors.
def negate(buf, max_col, offset):
    
    # Loop for every third number given an offset. 
    for i in range(len(buf)/3):
        
        # Use maximum color depth to negate color value. 
        buf[i*3 + offset] = max_col - buf[i*3 + offset]
    return buf

# Negate red values. 
def negate_red(buf, max_col):
    return negate(buf, max_col, 0)

# Negate green values.
def negate_green(buf, max_col):
    return negate(buf, max_col, 1)

# Negate blue values.
def negate_blue(buf, max_col):
    return negate(buf, max_col, 2)
    
# Add a mat num_pix wide of color pix (RGB values) to buffer buf.
def make_mat(buf, row_size, num_pix, pix):
    
    # Make a section of the mat (num_pix pixels in sequence)
    mat = []
    for x in range(num_pix):
        for y in pix:
            mat.append(y)
    
    # If no buffer, make a whole row of (RGB)
    if buf is None:
        buf = [y for x in range(row_size) for y in pix]
    
    # Add mat to beggining and end of line of pixels. 
    buf[0:0] = mat
    buf.extend(mat)
    
    return buf

# Gray scale edit. 
def gray_scale(buf):
    # Loop over every group of RGB values.
    for i in range(len(buf)/3):
        
        # Change RGB values to their average. 
        avg = buf[i*3] + buf[i*3+1] + buf[i*3+2]
        buf[i*3] = avg
        buf[i*3+1] = avg
        buf[i*3+2] = avg
    return buf

# Flip an image horizontally. 
def flip_horizontal(buf):
    
    # Loop over every group of RGB values. 
    end = len(buf)/3
    
    # Only flip values until half the image is reached. 
    for i in range(end/2):
        
        # Switch RGB values that mirror/are across from each other.
        temp = buf[i*3:(i+1)*3]
        buf[i*3:(i+1)*3] = buf[(end-i-1)*3 : (end-i)*3]
        buf[(end-i-1)*3 : (end-i)*3] = temp
    return buf   

# Blur the image horizontally. 
def horizontal_blur(buf):
    
    # 3 values per pixel, 3 pixels at a time. Average reds, greens,
    # and blues across 3 pixels.  
    for i in range(len(buf)/9):
        
        # For R, G, and B
        for k in range(3):
            avg = (buf[i*9+k] + buf[i*9+k+3] + buf[i*9+k+6])/3
            buf[i*9+k] = avg
            buf[i*9+k+3]  = avg
            buf[i*9+k+6] = avg
            
    # Special case for two pixels left over, don't do anything for one left over.
    if len(buf)%9 == 6:
        i = len(buf) - 6
        
        # For R, G, and B
        for k in range(3):
            avg = (buf[i+k] + buf[i+k+3])/2
            buf[i+k] = avg
            buf[i+k+3] = avg
    return buf

# Extreme contrast edit.                 
def extreme_contrast(buf, max_col):
    
    # Change values below midpoint of max color depth to 0.
    # Change those above it to max color depth. 
    for i in range(len(buf)):
        if buf[i] > max_col/2:
            buf[i] = max_col
        else:
            buf[i] = 0
    return buf
    
# This function dumps a buffer of pixels in a file. 
def buffLine(pixels, outf, size):
    i = 0
    
    # If and elif just in case pixels has any leftover values. 
    if len(pixels)%3 == 1:
        del pixels[len(pixels)-1]
    elif len(pixels)%3 == 2:
        del pixels[len(pixels)-1]
        del pixels[len(pixels)-2]
    
    # Change values to string and write them in the file at the correct dimentions. 
    for num in pixels:
        outf.write(str(num))
        outf.write(" ")
        i = i+1
        if i == size:
            i = 0
            outf.write('\n')

# Main processing function. 
def process_image(in_file, out_file, choices, mat_size, mat_col):
    
    # Open the input file.
    inf = open(in_file, "r")
    
    # Input the first line for later. 
    first_line = inf.readline()
    
    # Get image columns and rows.
    pic_size = inf.readline().split()
    cols = int(pic_size[0])
    rows = int(pic_size[1])
       
    # Check that there is space in the buffer: 
    # number of pixels per line of image * three integers per pixel
    row_size = cols * 3
    if row_size > BUF_SIZE:
        print "A row of image has more pixels than the current buffer can hold; please increase the buffer size."
        inf.close()
        return False
    
    # Pass along the header.
    outf = open(out_file, "w")
    outf.write(first_line)
    
    # Adjust for the mat, if any.
    outf.write(str(cols+mat_size*2))
    outf.write(" ")
    outf.write(str(rows+mat_size*2))
    outf.write('\n')
    third_line = inf.readline()
    outf.write(third_line)
    
    # Get color depth. 
    max_col = int(third_line)
    
    # Can now validate the RGB values for the mat.
    for col in mat_col:
        if int(col) < 0 or int(col) > max_col:
            print "Invalid color value for the mat!"
            return False 
    
    # Process the body. 
    values = inf.readline().split()
    
    # Remember this for output formatting.
    line_size = len(values)
    
    # Pre-Processing: print the top of the mat, if any.
    for i in range(mat_size):
        buffLine(make_mat(None, cols, mat_size, mat_col), outf, line_size+mat_size*6)

    # Process until end of file.
    while len(values) > 0:
        
        # Clean buffer
        buf = []
        size = len(values)
        
        # Only fill buffer up to a row of pixels. 
        while len(values) > 0 and size <= row_size:
            buf.extend(int(val) for val in values)
            values = inf.readline().split()
            size = size + len(values)
        
        # Make sure to fill to row size from the last values read. 
        buf.extend(int(val) for val in values[:len(values)-(size-row_size)])
        values = values[len(values)-(size-row_size):]
        
        # Process buffer according to user's choices.
        # Leave the mat last. 
        for ch in choices:
            if ch == 1:
                gray_scale(buf)
            elif ch == 2:
                flip_horizontal(buf)
            elif ch == 3:
                negate_red(buf, max_col)
            elif ch == 4:
                negate_green(buf, max_col)
            elif ch == 5:
                negate_blue(buf, max_col)
            elif ch == 7:
                horizontal_blur(buf)
            elif ch == 8:
                extreme_contrast(buf, max_col)
        
        # Now deal with the mat, if any.
        if mat_size > 0:
            make_mat(buf, cols, mat_size, mat_col)
        
        # Dump the buffer into the file. 
        buffLine(buf, outf, line_size+mat_size*6)
        
    # Post-Processing: print the bottom of the mat, if any.
    for i in range(mat_size):
        buffLine(make_mat(None, cols, mat_size, mat_col), outf, line_size+mat_size*6)
 
    # Close files. 
    inf.close()
    outf.close()
    return True
 