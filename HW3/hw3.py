#********************************************
# Christina Floristean 
# cf2469
# October 23, 2014
# Assignment 3
#
#
# Description: This program contains a user
# interface that allows the user to edit an 
# image from a list of editing options. It 
# implements image_shop to process the image 
# and carry out these edits. 
#********************************************

import image_shop

# User interface. 
def display_menu():
    
    # Header.
    print("PPM Image Editor")
    
    # Get the file names.
    in_file = raw_input("Enter image file path/name: ")
    out_file = raw_input("Enter output image file path/name: ")
    
    # Handle processing choices.
    print "\nHere are the processing choices available:"
    print "[1]  convert to grey scale"
    print "[2]  flip horizontally"
    print "[3]  negative of red"
    print "[4]  negative of green"
    print "[5]  negative of blue"
    print "[6]  create a mat"
    print "[7]  horizontal blur"
    print "[8]  extreme contrast"
    
    not_valid = True
    
    # Loop through in case of invalid inputs. 
    while not_valid:
        
        # Input all choices, split into list, and change to int values. 
        choices = [int(x) for x in raw_input("Enter the numbers of all those you want applied to the file separated by a space: ").split()]
        
        # Make sure choices are valid. 
        for ch in choices:
            if ch not in range(1,9):
                print "Invalid choice", ch
                not_valid = True
                break
            else:
                not_valid = False
    mat_size = 0
    mat_col = []
    
    # Extra input if mat is created. 
    if 6 in choices:
        
        # Get mat size.
        not_valid = True
        while not_valid:
            mat_size = input("Please enter the mat width in pixels: ")
            if mat_size <= 0:
                print "Positive value needed!"
            else:
                not_valid = False
        
        # Get mat color.
        not_valid = True
        while not_valid:
            mat_col = raw_input("Please enter R G B values for the mat color (space separated): ")
            mat_col = mat_col.split()
            if len(mat_col) != 3:
                print "Need three values!"
            else:
                not_valid = False
                
    # Process image.           
    if image_shop.process_image(in_file, out_file, choices, mat_size, mat_col):
        print("Output file generated!")

# Main function.
def main():
    # Start with the user interface.
    display_menu()
    
# Run main(). 
main()
    